import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import static java.lang.Math.abs;

public class Rover {

    public static void calculateRoverPath(int[][] map) {

        Point[][] points = new Point[map.length][map[0].length];
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points[0].length; j++) {
                points[i][j] = new Point(i,j);
            }
        }
        points[0][0].setEnergyFromStart(0);

        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points[0].length; j++) {
                int stepEnergy = 0;
                if (j < points[0].length - 1) {
                    stepEnergy = 1 + abs(map[i][j + 1] - map[i][j]);
                    if ((points[i][j].getEnergyFromStart() + stepEnergy) < points[i][j + 1].getEnergyFromStart()) {
                        points[i][j + 1].setEnergyFromStart(points[i][j].getEnergyFromStart() + stepEnergy);
                    }
                }
                if (i < points.length - 1) {
                    stepEnergy = 1 + abs(map[i + 1][j] - map[i][j]);
                    if ((points[i][j].getEnergyFromStart() + stepEnergy) < points[i + 1][j].getEnergyFromStart()) {
                        points[i+1][j].setEnergyFromStart(points[i][j].getEnergyFromStart() + stepEnergy);
                    }
                }
                points[i][j].setIsChecked(true);
            }
        }
        int x = points.length-1;
        int y = points[0].length-1;
        int reverseEnergy = points[x][y].getEnergyFromStart();
        List<Point> shortPath = new ArrayList<>();
        shortPath.add(points[x][y]);

        while (reverseEnergy != 0 ) {
            for (Map.Entry<Point, Integer> ent: nearPoints(x,y, map, points).entrySet()){
                if((reverseEnergy-ent.getValue())==ent.getKey().getEnergyFromStart()){
                    reverseEnergy=ent.getKey().getEnergyFromStart();
                    x=ent.getKey().getI();
                    y=ent.getKey().getJ();
                    shortPath.add(ent.getKey());
                    break;
                }
            }
        }
        Collections.reverse(shortPath);
        try {
            formatAndWrite(shortPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<Point, Integer> nearPoints(int i, int j, int[][] map, Point[][] points) {
        Map<Point, Integer> near = new HashMap<>();
        if (i<points.length-1){
            near.put(points[i+1][j], 1+abs(map[i][j]-map[i+1][j]));
        }
        if (j<points[0].length-1){
            near.put(points[i][j+1], 1+abs(map[i][j]-map[i][j+1]));
        }
        if (i>0){
            near.put(points[i-1][j], 1+abs(map[i][j]-map[i-1][j]));
        }
        if (j>0){
            near.put(points[i][j-1], 1+abs(map[i][j]-map[i][j-1]));
        }
        return near;
    }

    public static void formatAndWrite(List<Point> path) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<path.size(); i++){
            if (i==path.size()-1){
                sb.append("["+path.get(i).getI()+"]["+path.get(i).getJ()+"]"+"\n");
                continue;
            }
            sb.append("["+path.get(i).getI()+"]["+path.get(i).getJ()+"]->");
        }
        sb.append("steps: "+(path.size()-1)+"\n");
        sb.append("fuel: "+path.get(path.size()-1).getEnergyFromStart());

        try (FileOutputStream fout = new FileOutputStream("path-plan.txt", false)){
            fout.write(sb.toString().getBytes());
        }
    }

    static class Point {
        private int energyFromStart;
        private boolean isChecked;
        private int i;
        private int j;

        public Point(int i, int j) {
            this.energyFromStart = 20000000;
            this.isChecked = false;
            this.i=i;
            this.j=j;
        }

        public int getEnergyFromStart() {
            return energyFromStart;
        }

        public void setEnergyFromStart(int e) {
            this.energyFromStart = e;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setIsChecked(boolean checked) {
            this.isChecked = checked;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }
    }

}
